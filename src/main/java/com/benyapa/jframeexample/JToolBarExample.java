/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 *
 * @author bwstx
 */
public class JToolBarExample {

    public static void main(final String[] args) {
        JFrame f = new JFrame("Tool Bar Example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(450, 250);

        JToolBar tool = new JToolBar();
        tool.setRollover(true);

        JButton btn = new JButton("File");

        tool.add(btn);
        tool.addSeparator();
        tool.add(new JButton("Edit"));
        tool.add(new JComboBox(new String[]{"Opt-1", "Opt-2", "Opt-3", "Opt-4"}));

        Container contentPane = f.getContentPane();
        contentPane.add(tool, BorderLayout.NORTH);

        JTextArea textArea = new JTextArea();

        JScrollPane mypane = new JScrollPane(textArea);
        contentPane.add(mypane, BorderLayout.EAST);

        f.setVisible(true);

    }

}
