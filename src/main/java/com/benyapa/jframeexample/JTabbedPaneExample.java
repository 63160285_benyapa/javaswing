/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 *
 * @author bwstx
 */
public class JTabbedPaneExample {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(400, 400);
        frame.setLayout(null);
        
        
        JTextArea ta1 = new JTextArea(200,200);
        JTextArea ta2 = new JTextArea(200,200);
        JTextArea ta3 = new JTextArea(200,200);
        
        JPanel p1 = new JPanel();
        p1.add(ta1);
        JPanel p2 = new JPanel();
        p2.add(ta2);
        JPanel p3 = new JPanel();
        p3.add(ta3);
        
        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(50, 50, 200, 200);
        tp.add("Main",p1);
        tp.add("Visit",p2);
        tp.add("Help",p3);
        frame.add(tp);
        
        frame.setVisible(true);
    }
    
}
