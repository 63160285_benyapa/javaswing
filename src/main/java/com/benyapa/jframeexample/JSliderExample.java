/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 *
 * @author bwstx
 */
public class JSliderExample {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        
        JSlider slider = new JSlider(JSlider.HORIZONTAL,0,50,25);
        
        JPanel panel  = new JPanel();
        panel.add(slider);
        frame.add(panel);
        
        frame.pack();
        frame.setVisible(true);
    }
    
}
