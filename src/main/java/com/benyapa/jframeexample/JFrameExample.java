/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author bwstx
 */
public class JFrameExample {

    public static void main(String s[]) {
        JFrame frame = new JFrame("JFrame Example");
        frame.setSize(300, 300);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());

        JLabel lbl = new JLabel("JFrame By Example");

        JButton btn = new JButton();
        btn.setText("Button");

        panel.add(lbl);
        panel.add(btn);
        frame.add(panel);

        frame.setVisible(true);
    }

}
