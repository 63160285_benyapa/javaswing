/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 *
 * @author bwstx
 */
public class JPopupMenuExample {

    public static void main(String[] args) {
        final JFrame f = new JFrame("PopupMenu Example");
        f.setSize(400, 400);

        final JPopupMenu popupmenu = new JPopupMenu("Edit");

        JMenuItem cut = new JMenuItem("Cut");
        JMenuItem copy = new JMenuItem("Copy");
        JMenuItem paste = new JMenuItem("Paste");

        popupmenu.add(cut);
        popupmenu.add(copy);
        popupmenu.add(paste);

        f.addMouseListener(new MouseAdapter() { //เมื่อมีการคลิกหรือกดเมาส์จะแสดงลิสต์สามอันด้านบน
            public void mouseClicked(MouseEvent e) {
                popupmenu.show(f, e.getX(), e.getY());
            }
        });
        f.add(popupmenu);
        f.setLayout(null);
        f.setVisible(true);
    }

}
