/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JCheckBox;
import javax.swing.JFrame;

/**
 *
 * @author bwstx
 */
public class JCheckBoxExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Check Box Example");
        f.setSize(400, 400);
        f.setLayout(null);

        JCheckBox checkb1 = new JCheckBox("C++");
        checkb1.setBounds(100, 100, 50, 50);
        f.add(checkb1);

        JCheckBox checkb2 = new JCheckBox("Java");
        checkb2.setBounds(100, 150, 100, 100);
        f.add(checkb2);

        f.setVisible(true);

    }

}
