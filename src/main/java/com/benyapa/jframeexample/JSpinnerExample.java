/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author bwstx
 */
public class JSpinnerExample {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Spinner Example");
        frame.setSize(300, 300);
        frame.setLayout(null);
        
        SpinnerModel value = new SpinnerNumberModel(5, 0, 10, 1); //(ค่าเริ่ม,น้อยสุด,มากสุด,เพิ่มขึ้น(จำนวน))
        
        JSpinner spin = new JSpinner(value);
        spin.setBounds(100, 100, 50, 30);
        
        frame.add(spin);
        
        frame.setVisible(true);
    }
    
}
