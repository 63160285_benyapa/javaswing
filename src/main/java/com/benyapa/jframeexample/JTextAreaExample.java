/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author bwstx
 */
public class JTextAreaExample {

    JTextAreaExample() {
        JFrame f = new JFrame("JTextArea Example");
        f.setSize(300, 300);
        f.setLayout(null);

        JTextArea area = new JTextArea("Welcome to javatpoint");
        area.setBounds(10, 30, 200, 200);
        f.add(area);

        f.setVisible(true);
    }

    public static void main(String[] args) {
        new JTextAreaExample();
    }

}
