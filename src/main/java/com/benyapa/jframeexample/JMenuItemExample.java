/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author bwstx
 */
public class JMenuItemExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Menu and MenuItem Example");
        f.setSize(400, 400);

        JMenuBar bar = new JMenuBar();

        JMenu menu = new JMenu("Menu");
        JMenu submenu = new JMenu("Sub Menu");

        JMenuItem i1, i2, i3, i4, i5;

        i1 = new JMenuItem("Item 1");
        i2 = new JMenuItem("Item 2");
        i3 = new JMenuItem("Item 3");
        i4 = new JMenuItem("Item 4");
        i5 = new JMenuItem("Item 5");
        
        menu.add(i1);
        menu.add(i2);
        menu.add(i3);
        menu.add(submenu);
        submenu.add(i4);
        submenu.add(i5);

        bar.add(menu);
        
        f.setJMenuBar(bar);
        f.setLayout(null);
        f.setVisible(true);
    }

}
