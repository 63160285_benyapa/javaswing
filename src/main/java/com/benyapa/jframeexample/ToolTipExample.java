/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author bwstx
 */
public class ToolTipExample {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        frame.setSize(300, 300);
        frame.setLayout(null);
        
        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        value.setToolTipText("Enter your Password");
        
        JLabel lbl = new JLabel("Password");
        lbl.setBounds(20, 100, 80, 30);
        
        frame.add(value);
        frame.add(lbl);
        
        frame.setVisible(true);
    }
    
}
