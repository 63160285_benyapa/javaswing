/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author bwstx
 */
public class JEditorPaneExample {

    public static void main(String[] args) {
        JFrame frame = new JFrame("JEditorPane Test");
        frame.setSize(400, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JEditorPane pane = new JEditorPane();
        pane.setContentType("text/plain");
        pane.setText("Sleeping is necessary for a healthy body."
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days.");

        frame.setContentPane(pane);

        frame.setVisible(true);
    }
}
