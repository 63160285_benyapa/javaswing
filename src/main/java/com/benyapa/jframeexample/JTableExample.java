/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author bwstx
 */
public class JTableExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Table Example");
        f.setSize(300, 400);

        String data[][] = {{"101", "Amit", "670000"},
                                {"102", "Jai", "780000"},
                                {"101", "Sachin", "700000"}};
        String col[] = {"ID", "NAME", "SARALY"};

        JTable jt = new JTable(data, col);
        jt.setBounds(30, 40, 200, 300);
        
        JScrollPane sp = new JScrollPane(jt);
        f.add(sp);

        f.setVisible(true);
        
        //ไม่มีsetLayoutเสมอ
    }

}
