/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JScrollBar;

/**
 *
 * @author bwstx
 */
public class JScrollBarExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Scrollbar Example");
        f.setSize(400, 400);

        JScrollBar s = new JScrollBar();
        s.setBounds(100, 100, 50, 100);
        f.add(s);
        f.setLayout(null);
        f.setVisible(true);
    }

}
