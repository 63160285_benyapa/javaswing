/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author bwstx
 */
public class JPanelExample {
    
    public static void main(String[] args) {
        JFrame f  = new JFrame("Panel Example");
        f.setSize(400, 400);
        f.setLayout(null);
        
        JPanel panel = new JPanel();
        panel.setBounds(40, 80, 200, 200);
        panel.setBackground(Color.GRAY);
        
        JButton btn1 = new JButton("Button 1");
        btn1.setBounds(50, 100, 80, 30);
        btn1.setBackground(Color.yellow);
        JButton btn2 = new JButton("Button 2");
        btn2.setBounds(100, 100, 80, 30);
        btn2.setBackground(Color.green);
        
        panel.add(btn1);
        panel.add(btn2);
        
        f.add(panel);
        
        f.setVisible(true);
    }
    
}
