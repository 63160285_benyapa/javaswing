/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author bwstx
 */
public class DisplayingImage extends Canvas {

    public void paint(Graphics g) {

        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("C:\\Users\\bwstx\\Downloads\\XOsX.gif");
        g.drawImage(i, 0 ,0, this);

    }

    public static void main(String[] args) {
        DisplayingImage m = new DisplayingImage();
        JFrame f = new JFrame("Displaying Image");
        f.add(m);
        f.setSize(450, 450);
        f.setVisible(true);
    }

}
