/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author bwstx
 */
public class JLabelExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Label Example");
        f.setSize(300, 300);
        f.setLayout(null);

        JLabel lbl1, lbl2;
        lbl1 = new JLabel("First Label");
        lbl2 = new JLabel("Second Label");

        lbl1.setBounds(50, 50, 100, 50);
        f.add(lbl1);
        lbl2.setBounds(50, 100, 100, 50);
        f.add(lbl2);

        f.setVisible(true);
    }

}
