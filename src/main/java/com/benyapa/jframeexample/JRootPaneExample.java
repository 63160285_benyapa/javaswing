/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRootPane;

/**
 *
 * @author bwstx
 */
public class JRootPaneExample {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JRootPane root = frame.getRootPane();

        //menu bar
        JMenuBar bar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.add("Open");
        menu.add("Close");

        bar.add(menu);

        root.setJMenuBar(bar);
        root.getContentPane().add(new JButton("Press Me"));

        frame.pack();
        frame.setVisible(true);

    }

}
