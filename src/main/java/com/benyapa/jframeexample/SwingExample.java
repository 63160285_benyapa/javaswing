/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author bwstx
 */
public class SwingExample {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setSize(400, 500);

        JButton b = new JButton("CLICK");
        b.setBounds(130, 100, 100, 40); //กรอบbutton
        f.add(b);

        f.setLayout(null);
        f.setVisible(true);
    }

}
