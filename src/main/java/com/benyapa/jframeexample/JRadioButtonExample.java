/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

/**
 *
 * @author bwstx
 */
public class JRadioButtonExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Radio Button Example");
        f.setSize(400, 400);
        f.setLayout(null);

        JRadioButton r1 = new JRadioButton("A) Male");
        r1.setBounds(75, 50, 100, 30);
        f.add(r1);
        
        JRadioButton r2 = new JRadioButton("B) Female");
        r2.setBounds(75, 100, 100, 30);
        f.add(r2);
        
        ButtonGroup bg = new ButtonGroup();  //ตัวที่ทำให้เลือกได้ตัวเลือกเดียวเท่านั้น
        bg.add(r1);
        bg.add(r2);
        f.setVisible(true);
    }

}
