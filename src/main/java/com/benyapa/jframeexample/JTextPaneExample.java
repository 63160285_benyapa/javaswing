/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author bwstx
 */
public class JTextPaneExample {

    public static void main(String[] args) throws BadLocationException {
        JFrame frame = new JFrame("TextPane Example");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container con = frame.getContentPane();

        JTextPane tp = new JTextPane();
        tp.setText("Welcome");

        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setBold(attributeSet, true);

        tp.setCharacterAttributes(attributeSet, true);

        attributeSet = new SimpleAttributeSet();
        StyleConstants.setItalic(attributeSet, true);
        StyleConstants.setForeground(attributeSet, Color.RED);
        StyleConstants.setBackground(attributeSet, Color.YELLOW);

        Document doc = tp.getStyledDocument();
        doc.insertString(doc.getLength(), "To Java ", attributeSet);

        attributeSet = new SimpleAttributeSet(); // สร้างใหม่แยกคำระหว่าง tojava กับ world
        doc.insertString(doc.getLength(), "World", attributeSet);

        JScrollPane scrollPane = new JScrollPane(tp);
        con.add(scrollPane, BorderLayout.CENTER);

        frame.setVisible(true);
    }

}
