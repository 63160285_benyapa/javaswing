/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

/**
 *
 * @author bwstx
 */
public class JViewportExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Tabbed Pane Sample");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(400, 150);

        JLabel lbl = new JLabel("Label");
        lbl.setPreferredSize(new Dimension(1000, 1000));

        JButton btn = new JButton();

        JScrollPane sp = new JScrollPane(lbl);
        sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        sp.setViewportBorder(new LineBorder(Color.RED));
        sp.getViewport().add(btn, null);
        
        f.add(sp,BorderLayout.CENTER);
        f.setVisible(true);
    }

}
