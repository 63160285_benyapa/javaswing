/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JComboBox;
import javax.swing.JFrame;

/**
 *
 * @author bwstx
 */
public class JComboBoxExample {
    
    public static void main(String[] args) {
        JFrame f = new JFrame("Combo Box Example");
        f.setSize(400,500);
        f.setLayout(null);
        
        String country [] = {"India","Aus","U.S.A","England","Newzealand"};
        JComboBox combo = new JComboBox(country);
        combo.setBounds(50, 50, 90, 20);
        
        f.add(combo);
        f.setVisible(true);
        
    }
    
}
