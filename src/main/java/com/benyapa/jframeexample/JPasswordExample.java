/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author bwstx
 */
public class JPasswordExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Password Field Example");
        f.setSize(400, 400);

        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        f.add(value);

        JLabel lbl = new JLabel("Password");
        lbl.setBounds(20, 100, 80, 30);
        f.add(lbl);

        f.setLayout(null);
        f.setVisible(true);

    }

}
