/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.jframeexample;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author bwstx
 */
public class JButtonExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Button Example");
        f.setSize(400, 400);
        f.setLayout(null);

        JButton b = new JButton("CLICK HERE");
        b.setBounds(50, 100, 200, 35);
        f.add(b);
        f.setVisible(true);

    }

}
